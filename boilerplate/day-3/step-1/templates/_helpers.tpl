{{/*
Expand the name of the chart.
*/}}
{{- define "MyAppCtx.name" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s" $name | trunc 63 | trimSuffix "-"}}
{{- end }}
